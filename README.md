# NS-Consumentenzuil

Mini project voor prog

# Beschrijving van het project

Het project bestaat uit vier python programma's ondersteund door een paar extra files (logo,text files etc)
Nummer 2,3 en 4 gebruiken elk een Tkinter GUI

Het doel van dit project is om reizigers de kans te bieden on op twitter hun mening te geven over de NS.
Er word gebruikt gemaakt van het Twitter API.

1. RUN Parralel vanuit Pycharm

Dit programma verzameld de andere 3 codes en runt deze parralel dmv multiprocessing en de os module

2. Invoerzuil

In dit programma kan de reiziger in een GUI een twitter bericht invoeren, en deze naar een NS medewerker versturen voor keuring.

3. Personeel Paneel

In het personeel paneel krijgt de NS mederwerker het nieuwste bericht te zien, deze kan dan worden goedgekeurd, of afgekeurd. 
Als het bericht is afgekeurd word deze weggeschreven in een log.

4. Twitter Display

Dit is een simpele GUI omgeving waarin de laatste drie posts op het NS twitter account worden opgevraagd en gedisplayed.
Als er onvoldoende berichten zijn gepost wacht het programma een kwartier en probeert opnieuw om de berichten te displayen.

# Installeren pip
Voor het installeren van pip onder Windows kan je het volgende command gebruiken
```
pip install --user pipenv
```
```
python -m pip install --upgrade 
```

# Requirements
python pip met onderstaande;
- twython 
- tkinter
- datetime
- multiprocessing
- os (?)

Alleen Twython moet via de terminal worden geinstalleerd met pip3, alle andere modules zijn built-in en kunnen dus gewoon worden aangesproken.

- om Twython te installeren: pip install twython
