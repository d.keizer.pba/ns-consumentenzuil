#toegevoegd op 22-10-2020, automagisch install
import pip
#non standart module install
pip.main(['install','twython'])
pip.main(['install', 'Pillow'])
#module meerdere schermen tegelijkertijd
import multiprocessing
import os

#tuple met alle processen
all_processes = ('NS_personeel_paneel.py','NS_invoer_zuil.py','NS_twitter_display.py')


def execute(process):
    """Execute elk proces in de tuple"""
    os.system(f'python {process}')

#keep alive van de processen
if __name__ == '__main__':
    process_pool = multiprocessing.Pool(processes=3)
    process_pool.map(execute, all_processes)